FROM ubuntu
COPY my_first_script.sh /opt
WORKDIR /opt
CMD [bash my_first_script.sh]
